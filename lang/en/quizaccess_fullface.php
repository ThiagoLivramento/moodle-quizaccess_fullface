<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for quizacess_fullface
 *
 * @package   quizacess_fullface
 * @copyright 2019 Daniel Neis Araujo <daniel@adapta.online>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['apiurl'] = 'API URL ';
$string['enable'] = 'Enable authentication with FULLFace';
$string['enabledesc'] = 'This will enable authentication with FULLFace biometrics authentication services.';
$string['fullfacerequest'] = 'FullFace Request';
$string['fullfacerule'] = 'Authenticate with FULLFace';
$string['password'] = 'Password';
$string['pluginname'] = 'FULLFace quiz access rule';
$string['privacy:metadata'] = 'The FULLFace quiz access rule plugin does not store any personal data but shares the user\'s username with FULLFace service.';
$string['scope'] = 'Scope';
$string['secret'] = 'Secret';
$string['redirecturl'] = 'Redirect URL';
$string['time'] = 'Time between authentications';
$string['username'] = 'Username';
